from django.shortcuts import render
from projects.models import Project
from django.conf import settings
from django.shortcuts import redirect
# Create your views here.


def list_projects(request):
    if not request.user.is_authenticated:
        return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
    projects = Project.objects.filter(owner=request.user)
    context = {
        "Project": projects
        }
    return render(request, "projects/list.html", context)


def show_project(request, id):
    if not request.user.is_authenticated:
        return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
    project_details = Project.objects.get(id=id)
    context = {
        "project_details": project_details
    }
    return render(request, "projects/detail.html", context)
