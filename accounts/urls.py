from django.urls import path
from accounts.views import user_login, user_logout, signup


urlpatterns = [
    path("accounts/", user_login),
    path("accounts/", user_logout),
    path("login/", user_login, name="login"),
    path("logout/", user_logout, name="logout"),
    path("signup/", signup, name="signup")
]
